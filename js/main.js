    /*global $, console, alert, prompt */

/*Parallax
=====================*/

$(document).ready(function () {
    
    "use strict";
    
    var $window = $(window);
    
    $('[data-type="background"]').each(function () {
        
        var $bgobj = $(this);
        
        $(window).scroll(function () {
            
            var ypos = -($window.scrollTop() / $bgobj.data('speed')),
                
                coords = '50%' + ypos + 'px';
            
            $bgobj.css({
                
                backgroundPosition: coords
                
            });
            
        });
        
    });
    
});
// Proudect slider
$(document).ready(function () {
    "use strict";
    $(".product-slider .slider").slick({
        prevArrow: "",
        nextArrow: "",
        autoplay: true,
        autoplaySpeed: 2000
    });
});
// brand slider
$(document).ready(function () {
    "use strict";
    $(".brand.slider").slick({
        prevArrow: "",
        nextArrow: "",
        slidesToShow: 5,
        autoplay: true,
        autoplaySpeed: 2000,
        centerMode: true,
        centerPadding: '60px',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3,
                    variableWidth: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1,
                    variableWidth: false
                }
            }
        ]
    });
});
// brand slider
$(document).ready(function () {
    "use strict";
    $(".blog-head .slider").slick({
        prevArrow: "",
        nextArrow: "",
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        infinite: true,
        speed: 1000
    });
});
$(document).ready(function () {
    "use strict";
    $(".product-img-footer.slider").slick({
        prevArrow: "",
        nextArrow: "",
        slidesToShow: 4,
        slidesToScroll: 1,
        focusOnSelect: true,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3,
                    variableWidth: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1,
                    variableWidth: false
                }
            }
        ]
    });
});
// Add And Remove Active
$(document).ready(function () {
    "use strict";
    $("nav ul.navbar > li").click(function () {
        $(this).addClass("active").siblings("li").removeClass("active");
    });
    $(".page-head ul.btns > li").click(function () {
        $(this).addClass("active").siblings("li").removeClass("active");
    });
    $(".pick-size span figcaption").click(function () {
        $(this).toggleClass("active");
    });
    $(".product-img-footer .item").click(function () {
        $(this).addClass("active").siblings("div").removeClass("active");
        var v = $(this).html();
        $(".product-img-header .item").html(v);
    });
    $(".gallary-list li").click(function () {
        $(this).addClass("active").siblings("li").removeClass("active");
    });
    $('.allpages .icons').click(function() {
        $(".allpages").toggleClass("active");
    });

    // $('.edit-try').on('click',function(e){
    // 	// e.preventDefult();
    // 	var id = e.target.data('id');
    // 	$.ajax({
    // 		method : 'post',
    // 		data :{id:id,name:'msjjdjdj'},
    // 		url  :'/about.php'
    // 	}).;
    // });
    
    
});
// incremant & decrement order-num
$(document).ready(function () {
    "use strict";
    $(".order-num i.fa.fa-plus").click(function () {
        var $value = parseInt(($(this).closest('.order-num').find('span').html()), 10);
        $(".order-num span").html($value + 1);
    });
    
    $(".order-num i.fa.fa-minus").click(function () {
        var $value = parseInt(($(this).closest('.order-num').find('span').html()), 10);
        if ($value > 1) {
            $(".order-num span").html($value - 1);
        }
        
    });
    $(".shopping .table tbody tr td .quantity i.fa.fa-plus").click(function () {
        var $value = parseInt(($(this).closest('.quantity').find('span').html()), 10);
        $(this).closest('.quantity').find('span').html($value + 1);
    });
    $(".shopping .table tbody tr td .quantity i.fa.fa-minus").click(function () {
        var $value = parseInt(($(this).closest('.quantity').find('span').html()), 10);
        if ($value > 1) {
            $(this).closest('.quantity').find('span').html($value - 1);
        }
    });
});
// remove media
$(document).ready(function () {
    "use strict";
    $(".media").find("i.fa.fa-times").click(function () {
        $(this).closest("tr").fadeOut();
    });
});
/* Nice Scroll
===============================*/
$(document).ready(function () {
    
    "use strict";
    
	$("html").niceScroll({
        scrollspeed: 60,
        mousescrollstep: 35,
        cursorwidth: 5,
        cursorcolor: '#FFAE00',
        cursorborder: 'none',
        background: 'rgba(255,255,255,0.3)',
        cursorborderradius: 3,
        autohidemode: false,
        cursoropacitymin: 0.1,
        cursoropacitymax: 1,
        zindex: "999",
        horizrailenabled: false
	});
});
/* loading
====================== */

$(document).ready(function () {
    "use strict";
    $(window).load(function () {
        $(".loading-overlay .spinner").fadeOut(200, function () {
            $("body").css("overflow", "auto");
            $(this).parent().fadeOut(200, function () {
                $(this).remove();
            });
        });
    });
});
// MixItUP
$(document).ready(function () {
    "use strict";
    var mixer = mixitup('.products-tabs');
     $(".navbar-toggle").click(function (){
         $(this).removeClass("mixitup-control-active");
         $(".products-tabs").removeClass("mixitup-container-failed");
     });
  
    // mixitup-control-active
    // mixitup-container-failed
});


