var map;
function initMap() {
    "use strict";
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: new google.maps.LatLng(-33.91722, 151.23064),
        mapTypeId: 'roadmap'
    });

    var iconBase = 'http://i.imgur.com/',
        icons = {
            info: {
                icon: iconBase + 'ubPI0cp.png'
            }
        };

    var features = [
            {
                position: new google.maps.LatLng(-33.91721, 151.22630),
                type: 'info'
            }
        ];

        // Create markers.
    features.forEach(function (feature) {
        var marker = new google.maps.Marker({
            position: feature.position,
            icon: icons[feature.type].icon,
            map: map
        });
    });
}
